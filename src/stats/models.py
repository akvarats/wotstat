# coding:utf-8

from __future__ import unicode_literals

from django.db import models

__author__ = 'akvarats'


class StatValuesModel(models.Model):
    u"""
    Миксин для данных статистики
    """
    # количество боев (всего, побед, поражений и ничей)
    battles = models.PositiveIntegerField(default=0)
    wins = models.PositiveIntegerField(default=0)
    losses = models.PositiveIntegerField(default=0)
    draws = models.PositiveIntegerField(default=0)

    # урон (нанесено и получено)
    damage = models.PositiveIntegerField(default=0)
    damage_received = models.PositiveIntegerField(default=0)

    # очки захвата и сбития базы
    def_points = models.PositiveIntegerField(default=0)
    capture_points = models.PositiveIntegerField(default=0)

    # обнаружено противников
    spotted = models.PositiveIntegerField(default=0)

    # выстрелов
    shots = models.PositiveIntegerField(default=0)

    # попадания (нанесено, получено)
    hits = models.PositiveIntegerField(default=0)

    # пробитий (нанесено и получено)
    piercings = models.PositiveIntegerField(default=0)
    piercings_received = models.PositiveIntegerField(default=0)

    # выжил в боях
    survived_battles = models.PositiveIntegerField(default=0)

    # суммарный опыт
    xp = models.PositiveIntegerField(default=0)

    # получено прямых попаданий
    hits_received = models.PositiveIntegerField(default=0)
    hits_received_no_damage = models.PositiveIntegerField(default=0)

    # средний заблокированный урон
    avg_damage_blocked = models.DecimalField(max_digits=10, decimal_places=4)

    # танкинг фактор
    tanking_factor = models.DecimalField(max_digits=10, decimal_places=4)

    class Meta:
        abstract = True


class UserStat(StatValuesModel):

    user = models.ForeignKey('accounts.User')
    at = models.DateTimeField()

    global_raiting = models.PositiveIntegerField(default=0)
    max_xp = models.PositiveIntegerField(default=0)
    max_damage = models.PositiveIntegerField(default=0)
    last_battle_time = models.DateTimeField(null=True)
    logout_at = models.DateTimeField(null=True)

    class Meta:
        db_table = 'user_stats'


class UserStatRaw(models.Model):
    u""" """

    stat = models.OneToOneField(UserStat, related_name='raw')
    body = models.TextField()

    class Meta:
        db_table = 'user_stats_raw'


class UserTankStat(StatValuesModel):
    u""" """

    user = models.ForeignKey('accounts.User')
    tank = models.ForeignKey('wargaming.WGTank')
    at = models.DateTimeField()

    mark_of_mastery = models.PositiveSmallIntegerField(default=0)
    max_frags = models.PositiveSmallIntegerField(default=0)
    max_xp = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = 'user_tank_stats'


class UserTankStatRaw(models.Model):

    stat = models.ForeignKey(UserTankStat, related_name='raw')
    body = models.TextField()

    class Meta:
        db_table = 'user_tank_stats_raw'


# ----------------------------------------------------------------------------------------------------------------------
# Дневные срезы статистики
# ----------------------------------------------------------------------------------------------------------------------
class UserStatDayCut(models.Model):
    u"""
    Дневной срез статистики игрока
    """
    user = models.ForeignKey('accounts.User')
    day = models.DateField()
    stat = models.ForeignKey(UserStat)

    class Meta:
        db_table = 'user_stats_day_cut'


class UserTankStatDayCut(models.Model):
    u"""
    Дневной срез статистики игрока по танку
    """
    user = models.ForeignKey('accounts.User')
    day = models.DateField()
    tank = models.ForeignKey('wargaming.WGTank')

    class Meta:
        db_table = 'user_tank_stats_day_cut'

