# coding:utf-8

from __future__ import unicode_literals
from stats.models import UserTankStat, UserStat

__author__ = 'akvarats'


def map_wotstat_to_userstat(wot_stat, user_stat):
    u""" """
    assert isinstance(wot_stat, dict)
    assert isinstance(user_stat, (UserStat, UserTankStat))

    user_stat.battles = wot_stat['battles']
    user_stat.wins = wot_stat['wins']
    user_stat.losses = wot_stat['losses']
    user_stat.draws = wot_stat['draws']

    user_stat.damage = wot_stat['damage_dealt']
    user_stat.damage_received = wot_stat['damage_received']

    user_stat.def_points = wot_stat['dropped_capture_points']
    user_stat.capture_points = wot_stat['capture_points']

    user_stat.spotted = wot_stat['spotted']

    user_stat.shots = wot_stat['shots']

    user_stat.hits = wot_stat['hits']

    user_stat.piercings = wot_stat['piercings']
    user_stat.piercings_received = wot_stat['piercings_received']

    user_stat.survived_battles = wot_stat['survived_battles']

    user_stat.xp = wot_stat['xp']

    user_stat.hits_received = wot_stat['direct_hits_received']
    user_stat.hits_received_no_damage = wot_stat['no_damage_direct_hits_received']

    user_stat.avg_damage_blocked = wot_stat['avg_damage_blocked']
    user_stat.tanking_factor = wot_stat['tanking_factor']

    return user_stat