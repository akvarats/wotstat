# coding:utf-8

from __future__ import unicode_literals

from django.conf.urls import url

from stats.views.account_stats import StatsAccountLatestView, StatsAccountHistoryView

__author__ = 'akvarats'

urlpatterns = [
    url(r'^api/stats/account-latest', StatsAccountLatestView.as_view()),
    url(r'^api/stats/account-history', StatsAccountHistoryView.as_view())
]
