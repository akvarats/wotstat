# coding:utf-8

from __future__ import unicode_literals
from accounts.api import user_today
from accounts.models import User
from stats.api.day_cut import get_user_day_stats, update_user_day_cut

__author__ = 'akvarats'


def account_today_stat(user, force_update=False):
    assert isinstance(user, User)

    today = user_today(user)

    if force_update:
        update_user_day_cut(user, today)

    day_stats = get_user_day_stats(user, today)

    if not force_update and not day_stats:
        # если за сегодняшний день статистика ещё не запрашивалась, то запрашиваем
        update_user_day_cut(user, today)
        day_stats = get_user_day_stats(user, today)

    return day_stats
