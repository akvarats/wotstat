# coding: utf-8

from __future__ import unicode_literals

import datetime
import json
from accounts.models import User
from core.utils import from_unix_utc_datetime, utc_now
from core.wot_api import user_stat, user_tanks_stat
from django.db import transaction
from stats.mappers import map_wotstat_to_userstat
from stats.models import UserStatDayCut, UserTankStatDayCut, UserStat, UserStatRaw, UserTankStat, UserTankStatRaw

__author__ = 'akvarats'


def get_user_day_stats(user, day):
    assert isinstance(user, User)
    assert isinstance(day, datetime.date)

    day_cut = UserStatDayCut.objects.filter(user=user, day=day).order_by('-id').first()

    return day_cut.stat if day_cut else None


def update_user_day_cut(user, day):
    u"""
    Обновляет данные по статистике дня игрока user

    :param User user: пользователь, для которого мы обновляем итоги дня
    :param date day: день, за который необходимо обновить итоги
    """
    assert isinstance(user, User)
    assert isinstance(day, datetime.date)

    now = utc_now()
    # загружаем статистику игрока
    wot_user_stat = user_stat(user.wot_id)
    stat_data = wot_user_stat['data'][str(user.wot_id)]
    wot_tanks_stat = user_tanks_stat(user.wot_id)
    tanks_data = wot_tanks_stat['data'][str(user.wot_id)]

    with transaction.atomic():

        # удаляем информацию о дневных итогах
        UserStatDayCut.objects.filter(day=day).delete()
        UserTankStatDayCut.objects.filter(day=day).delete()

        # сохраняем статистику по игроку вцелом
        stat = UserStat()
        stat.user = user
        stat.at = now
        stat.global_raiting = stat_data['global_rating']
        stat.logout_at = from_unix_utc_datetime(stat_data['logout_at'])
        stat.last_battle_time = from_unix_utc_datetime(stat_data['last_battle_time'])

        map_wotstat_to_userstat(stat_data['statistics']['all'], stat)
        stat.max_xp = stat_data['statistics']['all']['max_xp']
        stat.max_damage = stat_data['statistics']['all']['max_damage']

        stat.save()

        stat_raw = UserStatRaw()
        stat_raw.stat = stat
        stat_raw.body = json.dumps(stat_data)
        stat_raw.save()

        day_cut = UserStatDayCut()
        day_cut.user = user
        day_cut.day = day
        day_cut.stat = stat
        day_cut.save()

        # сохраняем статистику по танкам
        for tank_data in tanks_data:

            tank_stat = UserTankStat()
            tank_stat.user = user
            tank_stat.at = now
            tank_stat.tank_id = tank_data['tank_id']
            tank_stat.max_xp = tank_data['max_xp']
            tank_stat.max_frags = tank_data['max_frags']
            tank_stat.mark_of_mastery = tank_data['mark_of_mastery']
            map_wotstat_to_userstat(tank_data['all'], tank_stat)

            try:
                tank_stat.save()
            except:
                continue

            tank_stat_raw = UserTankStatRaw()
            tank_stat_raw.stat = tank_stat
            tank_stat_raw.body = json.dumps(tank_data)
            tank_stat_raw.save()

            tank_day_cut = UserTankStatDayCut()
            tank_day_cut.user = user
            tank_day_cut.day = day
            tank_day_cut.stat = tank_stat
            tank_day_cut.tank_id = tank_data['tank_id']
            tank_day_cut.save()