# coding:utf-8

from __future__ import unicode_literals
import math

from core.wot_api import user_tanks_stat
from django.core.management import BaseCommand

__author__ = 'akvarats'


class Command(BaseCommand):

    def handle(self, *args, **options):

        expDmg = 1446.47
        expSpot = 1.68
        expFrag = 0.94
        expDef = 0.83
        expWinRate = 51.37

        # expDmg = 691.84
        # expSpot = 1.29
        # expFrag = 0.92
        # expDef = 0.82
        # expWinRate = 53.09

        wot_user_id = 36822929

        tanks_stat = user_tanks_stat(wot_user_id)

        t54_stat = None
        for row in tanks_stat['data'][str(wot_user_id)]:
            if row['tank_id'] == 7937:
                t54_stat = row['all']
                break

        if tanks_stat:
            total_battles = t54_stat['battles']
            avgDmg = 1.0 * t54_stat['damage_dealt'] / total_battles
            avgSpot = 1.0 * t54_stat['spotted'] / total_battles
            avgFrag = 1.0 * t54_stat['frags'] / total_battles
            avgDef = 1.0 * t54_stat['dropped_capture_points'] / total_battles
            avgWinRate = 100.0 * t54_stat['wins'] / total_battles
            avgZahv = 1.0 * t54_stat['capture_points'] / total_battles

            rDAMAGE = 1.0 * avgDmg  / expDmg
            rSPOT   = 1.0 * avgSpot / expSpot
            rFRAG   = 1.0 * avgFrag    / expFrag
            rDEF    = 1.0 * avgDef     / expDef
            rWIN    = 1.0 * avgWinRate / expWinRate

            rWINc    = max(0,                     (rWIN    - 0.71) / (1 - 0.71) )
            rDAMAGEc = max(0,                     (rDAMAGE - 0.22) / (1 - 0.22) )
            rFRAGc   = max(0, min(rDAMAGEc + 0.2, (rFRAG   - 0.12) / (1 - 0.12)))
            rSPOTc   = max(0, min(rDAMAGEc + 0.1, (rSPOT   - 0.38) / (1 - 0.38)))
            rDEFc    = max(0, min(rDAMAGEc + 0.1, (rDEF    - 0.10) / (1 - 0.10)))

            WN8 = 980*rDAMAGEc + 210*rDAMAGEc*rFRAGc + 155*rFRAGc*rSPOTc + 75*rDEFc*rFRAGc + 145*min(1.8,rWINc)

            print 'WN8:', WN8

            # рейтинг эффективности
            K_d = avgDmg * (10.0 / (2 + 9)) * (0.23 + 0.02 * 9)
            K_f = 250 * avgFrag
            K_o = 150 * avgSpot
            K_z = 150 * math.log(avgZahv + 1, 1.732)
            K_s = 150 * avgDef

            RE = K_d + K_f + K_o + K_z + K_s

            print 'РЭ:', RE

