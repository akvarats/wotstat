# coding:utf-8

from __future__ import unicode_literals

import datetime
from accounts.models import User
from django.core.management import BaseCommand
from stats.api.day_cut import update_user_day_cut

__author__ = 'akvarats'


class Command(BaseCommand):
    u""" """

    def handle(self, *args, **options):

        day = datetime.date.today()

        for user in User.objects.all():
            update_user_day_cut(user, day)
