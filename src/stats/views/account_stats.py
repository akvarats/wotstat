# coding:utf-8

from __future__ import unicode_literals
from core.auth import check_api_auth
from core.casts import datetime_to_string
from core.request import request_body, request_param
from core.response import json_response, bad_request_response
from django.views.generic import View
from stats.api.account_stats import account_today_stat

__author__ = 'akvarats'


# ----------------------------------------------------------------------------------------------------------------------
# Последняя актуальная статистика по аккаунту
# ----------------------------------------------------------------------------------------------------------------------
class StatsAccountLatestView(View):
    u""" """

    @check_api_auth
    def get(self, request):

        # разбираем параметры запроса
        body = request_body(request)
        need_update = request_param(body, 'update', 'bool', default=False)

        # получаем статистику игрока на сегодняшний день
        day_stats = account_today_stat(request.user, need_update)

        if not day_stats:
            result = dict()
        else:
            result = dict(
                stats=dict(
                    battles=day_stats.battles,
                    wins=day_stats.wins,
                    losses=day_stats.losses,
                    draws=day_stats.draws,
                    wg=day_stats.global_raiting,

                    winrate=round(100.0 * day_stats.wins / day_stats.battles, 2)
                ),
                updated_at=datetime_to_string(day_stats.at)
            )

        return json_response(result)


# ----------------------------------------------------------------------------------------------------------------------
# История статистики по аккаунту
# ----------------------------------------------------------------------------------------------------------------------
class StatsAccountHistoryView(View):
    """
    Вьюха для истории статистики по аккаунту
    """

    @check_api_auth
    def get(self, request):
        """
        Возвращает историю статистики по аккаунту в виде списка объектов вида

        {
            "period": {
                "group_by": "day" или "week" или "month" или "quarter" или "year" или "any",
                "since": "01.01.2016",
                "until": "31.01.2016",
                "name": "01.01.2016 (пн)" или "31 мар - 07 апр 2016г." или "Май 2016 г." или "1 квартал 2016 г." или "2016 г.",
            },
            "cut": {
                "battles": 1000,
                "wins": 1000,
                "losses": 1000,
                "winrate": 1000,
                "wn8": 1000,
                "eff": 200
            },
            "inner": {
                "battles": 50,
                "wins": 10,
                "looses": 40,
                "winrate": 20.00,
                "wn8": 1000,
                "eff": 2000
            },
            "delta": {
                "battles": 50,
                "wins": 10,
                "losses": 40,
                "winrate": -0.02,
                "wg": -200,
                "wn8": 10.23,
                "eff": -29.3
            }
        }
        """
        body = request_body(request)

        since = request_param(body, 'since', 'date', None)
        until = request_param(body, 'until', 'date', None)
        group_by = request(body, 'group_by', 'str', None)

        if since and until and since > until:
            return bad_request_response('Дата since не может быть больше даты until')

        if group_by and group_by not in ('day', 'week', 'month', 'quarter', 'year'):
            return bad_request_response('Значение group_by должно быть указано из списка: day, week, month, '
                                        'quarter, year')

        if not group_by:
            # по дефолту группируем данные по дням
            group_by = 'day'

        return json_response([])