# coding:utf-8

from __future__ import unicode_literals
import abc

__author__ = 'akvarats'

class IRatingSourceRow(object):
    u""" """

    @abc.abstractproperty
    def level(self):
        u""" Уровень техники """

    @abc.abstractproperty
    def battles(self):
        u""" Количество боев """

    @abc.abstractproperty
    def wins(self):
        u""" Количество побед """

    @abc.abstractproperty
    def frags(self):
        u""" Фраги """

    @abc.abstractproperty
    def damage(self):
        u""" Нанесенный урон """

    @abc.abstractproperty
    def spotted(self):
        u""" Обнаружено противников """

    @abc.abstractproperty
    def def_points(self):
        u""" Очки защиты базы """

    @abc.abstractproperty
    def capture_points(self):
        u""" Очки захвата базы """

    @property
    def win_rate(self):
        return (100.0 * self.wins / self.battles) if self.battles > 0 else 0

    @property
    def avg_damage(self):
        return (1.0 * self.damage / self.battles) if self.battles > 0 else 0

    @property
    def avg_spotted(self):
        return (1.0 * self.spotted / self.battles) if self.battles > 0 else 0

    @property
    def avg_frag(self):
        return (1.0 * self.frags / self.battles) if self.battles > 0 else 0

    @property
    def avg_def(self):
        return (1.0 * self.def_points / self.battles) if self.battles > 0 else 0

    @property
    def avg_capture(self):
        return (1.0 * self.capture_points / self.battles) if self.battles > 0 else 0