# coding:utf-8

from __future__ import unicode_literals
from .base import *

__author__ = 'akvarats'


DEBUG = False

WSGI_APPLICATION = 'wotstat.wsgi.production.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'wotstat',
        'USER': 'wotstat',
        'PASSWORD': 'wotstat',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}

ALLOWED_HOSTS = ['wot.akvarats.me']

WOT_APPLICATION_ID = 'edcd6e1c355e96e8010d517f4755d138'

DOMAIN_URL = 'http://wot.akvarats.me/'

try:
    from .local import *
except ImportError:
    pass

