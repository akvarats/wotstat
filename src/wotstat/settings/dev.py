# coding:utf-8

from __future__ import unicode_literals
from .base import *

__author__ = 'akvarats'


DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

WSGI_APPLICATION = 'wotstat.wsgi.dev.application'

WOT_APPLICATION_ID = '815270acd6cefcbd103f8a6cf05f1dec'

DOMAIN_URL = 'http://127.0.0.1:7000/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'wotstat',
        'USER': 'wotstat',
        'PASSWORD': 'wotstat',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

try:
    from .local import *
except ImportError:
    pass



