# coding:utf-8

from __future__ import unicode_literals

from django.db import models

__author__ = 'akvarats'


class WGTank(models.Model):
    u"""
    Модель для хранения списка танков
    """

    tank_id = models.PositiveIntegerField(default=0)

    is_premium = models.BooleanField(default=False)
    level = models.PositiveSmallIntegerField(default=0)

    name = models.TextField(null=True)
    short_name = models.TextField(null=True)

    nation = models.TextField(null=True)

    type = models.TextField(null=True)

    class Meta:
        db_table = 'tanks'


class WGTankImage(models.Model):
    u"""
    Картинки для танков (хранятся в отдельной модели, чтобы не нагружать чтение)
    """

    tank = models.OneToOneField(WGTank, related_name='images')

    contour_image = models.TextField(null=True)
    image = models.TextField(null=True)
    image_small = models.TextField(null=True)

    class Meta:
        db_table = 'tank_images'


class WN8ETVVersion(models.Model):
    u"""

    """
    since = models.DateField(null=True)
    until = models.DateField(null=True)
    index = models.PositiveSmallIntegerField()

    class Meta:
        db_table = 'wn8_etv_version'


class WN8ETV(models.Model):
    u"""
    Таблица потанковых показателей для расчета WN8
    """

    version = models.ForeignKey('WN8ETVVersion', related_name='tanks')
    tank = models.ForeignKey(WGTank)

    exp_frag = models.DecimalField(max_digits=10, decimal_places=4)
    exp_damage = models.DecimalField(max_digits=10, decimal_places=4)
    exp_spot = models.DecimalField(max_digits=10, decimal_places=4)
    exp_def = models.DecimalField(max_digits=10, decimal_places=4)
    exp_winrate = models.DecimalField(max_digits=10, decimal_places=4)

    class Meta:
        db_table = 'wn8_etv'
