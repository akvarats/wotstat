# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-23 19:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WGTank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tank_id', models.PositiveIntegerField(default=0)),
                ('is_premium', models.BooleanField(default=False)),
                ('level', models.PositiveSmallIntegerField(default=0)),
                ('name', models.TextField(null=True)),
                ('short_name', models.TextField(null=True)),
                ('nation', models.TextField(null=True)),
                ('type', models.TextField(null=True)),
            ],
            options={
                'db_table': 'tanks',
            },
        ),
        migrations.CreateModel(
            name='WGTankImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contour_image', models.TextField(null=True)),
                ('image', models.TextField(null=True)),
                ('image_small', models.TextField(null=True)),
                ('tank', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='wargaming.WGTank')),
            ],
            options={
                'db_table': 'tank_images',
            },
        ),
        migrations.CreateModel(
            name='WN8ETV',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('exp_frag', models.DecimalField(decimal_places=4, max_digits=10)),
                ('exp_damage', models.DecimalField(decimal_places=4, max_digits=10)),
                ('exp_spot', models.DecimalField(decimal_places=4, max_digits=10)),
                ('exp_def', models.DecimalField(decimal_places=4, max_digits=10)),
                ('exp_winrate', models.DecimalField(decimal_places=4, max_digits=10)),
                ('tank', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='wargaming.WGTank')),
            ],
            options={
                'db_table': 'wn8_etv',
            },
        ),
        migrations.CreateModel(
            name='WN8ETVVersion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('since', models.DateField(null=True)),
                ('until', models.DateField(null=True)),
                ('index', models.PositiveSmallIntegerField()),
            ],
            options={
                'db_table': 'wn8_etv_version',
            },
        ),
        migrations.AddField(
            model_name='wn8etv',
            name='version',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tanks', to='wargaming.WN8ETVVersion'),
        ),
    ]
