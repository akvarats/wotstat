# coding: utf-8

from __future__ import unicode_literals
from optparse import make_option
from django.core.management import BaseCommand
from django.db import transaction

import requests
from wargaming.models import WN8ETVVersion, WGTank, WN8ETV

__author__ = 'akvarats'



class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option(
            '--source',
            action='store',
            dest='source',
            default=None,
            help='URL до JSON файла с данными'
        ),
    )

    def handle(self, *args, **options):

        source_url = options.get('source', None)
        if not source_url:
            self.stderr.write('Не указан источник данных (параметр --source)')
            return

        response = requests.get(source_url)

        if response.status_code != 200:
            self.stderr.write('Не удалось запросить данные по URL %s' % source_url)
            return

        wn8_etv_response = response.json()

        version = wn8_etv_response['header']['version']
        if not isinstance(version, int):
            self.stderr.write('Не удалось определить номер версии из источника %s' % source_url)
            return

        self.stdout.write('Версия WN8 ETV: %i' % version)

        wn8_etv_data = wn8_etv_response['data']
        if not isinstance(wn8_etv_data, list):
            self.stderr.write('Не удалось получить список потанковых значений из источника %s' % source_url)
            return

        self.stdout.write('Количество записей WN8 ETV: %i' % len(wn8_etv_data))

        # проверяем наличие определения танков
        tanks_map = dict()
        for tank_id in WGTank.objects.all().values_list('id', flat=True):
            tanks_map[tank_id] = tank_id

        with transaction.atomic():
            try:
                wn8_etv_version = WN8ETVVersion.objects.get(index=version)

                # удаляем старые данные и загружаем новые
                WN8ETV.objects.filter(version=wn8_etv_version).delete()
            except WN8ETVVersion.DoesNotExist:
                wn8_etv_version = WN8ETVVersion(index=version)
                wn8_etv_version.save()

            for etv_row in wn8_etv_data:
                if etv_row['IDNum'] in tanks_map:
                    wn8_etv = WN8ETV(version=wn8_etv_version)
                    wn8_etv.tank_id = etv_row['IDNum']
                    wn8_etv.exp_frag = etv_row['expFrag']
                    wn8_etv.exp_damage = etv_row['expDamage']
                    wn8_etv.exp_spot = etv_row['expSpot']
                    wn8_etv.exp_def = etv_row['expDef']
                    wn8_etv.exp_winrate = etv_row['expWinRate']

                    wn8_etv.save()
                else:
                    self.stdout.write('Танк с ID %s не найден' % etv_row['IDNum'])

        self.stdout.write('Данные успешно загружены')

