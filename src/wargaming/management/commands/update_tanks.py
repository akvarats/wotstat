# coding:utf-8

from __future__ import unicode_literals
from django.core.management import BaseCommand
from wargaming.api import update_tanks

__author__ = 'akvarats'


class Command(BaseCommand):
    u""" """

    def handle(self, *args, **options):
        update_tanks()