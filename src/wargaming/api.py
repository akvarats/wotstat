# coding:utf-8

from __future__ import unicode_literals
from core.wot_api import wot_tanks
from wargaming.models import WGTank, WGTankImage

__author__ = 'akvarats'


def update_tanks():
    u"""
    Выполняет обновление списка танков
    """

    tanks_data = wot_tanks()

    tanks_map = dict()
    for tank in WGTank.objects.all().select_related('images'):
        tanks_map[tank.id] = tank

    for _, tank_data in tanks_data['data'].iteritems():
        tank_id = tank_data['tank_id']
        if tank_id in tanks_map:
            tank = tanks_map[tank_id]
            tank_images = tank.images
        else:
            tank = WGTank(id=tank_id)
            tank_images = WGTankImage()

        # заполняем данные танка
        tank.tank_id = tank_id
        tank.is_premium = tank_data['is_premium']
        tank.level = tank_data['tier']
        tank.name = tank_data['name']
        tank.short_name = tank_data['short_name']
        tank.nation = tank_data['nation']
        tank.type = tank_data['type']

        # заполняем данные по картинке танка
        tank_images.contour_image = tank_data['images']['contour_icon']
        tank_images.image = tank_data['images']['big_icon']
        tank_images.image_small = tank_data['images']['small_icon']

        tank.save()
        tank_images.tank = tank
        tank_images.save()

    return

