# coding:utf-8

from __future__ import unicode_literals

import datetime

from accounts.models import User
from accounts.users import get_user_by_wot_id, access_token_is_active, update_access_token
from core.utils import from_unix_utc_datetime
from core.wot_api import wot_auth_prolongate

__author__ = 'akvarats'


class WotAuthBackend(object):

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def authenticate(self, wot_id):

        user = get_user_by_wot_id(wot_id)

        if not user or not access_token_is_active(user):
            user = None

        # Проверяем auth_token в wargaming

        prolongate_result = wot_auth_prolongate(user.access_token)
        if prolongate_result.get('status') != 'ok':
            return None

        data = prolongate_result.get('data')
        if not data or 'access_token' not in data or 'expires_at' not in data or 'account_id' not in data:
            return None

        if data['account_id'] != wot_id:
            # мы не можем использовать токены от других аккаунтов
            return None

        update_access_token(user, data['access_token'], from_unix_utc_datetime(data['expires_at']))

        return user
