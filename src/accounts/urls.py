from accounts.views.user_session import UserSessionView
from accounts.views.wot_auth import WotAuthLoginView, WotAuthLogonView
from django.conf.urls import url

__author__ = 'akvarats'

urlpatterns = [
    url(r'^api/user-session', UserSessionView.as_view()),
    url(r'^api/wot-auth/login', WotAuthLoginView.as_view()),
    url(r'^api/wot-auth/logon', WotAuthLogonView.as_view())
]