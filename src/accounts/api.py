# coding:utf-8

from __future__ import unicode_literals

import datetime

from core.utils import utc_now
from accounts.models import User


__author__ = 'akvarats'


def user_now(user):
    u""" Возвращает текущее время у пользователя """

    assert isinstance(user, User)

    return utc_now() + datetime.timedelta(minutes=user.gmt)


def user_today(user, shift_hours=4):
    u"""
    Возвращает текущий день пользователя (с учетом того, что день у пользователя смещен на 4 часа в сторону ночи)
    """
    assert isinstance(user, User)

    now = user_now(user)

    return now.date() if now.hour >= shift_hours else now.date() - datetime.timedelta(days=1)



