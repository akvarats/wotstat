# coding:utf-8

from __future__ import unicode_literals
import pytz
import datetime
from accounts.models import User

__author__ = 'akvarats'


def get_user_by_name(username):
    u"""
    Возвращает пользователя с указанным email
    """
    assert isinstance(username, basestring) and username

    return User.objects.filter(username=username).order_by('id').first()


def get_user_by_wot_id(wot_id):
    u"""
    Возвращает пользователя по идентификатору аккаунту в WoT
    """
    assert isinstance(wot_id, int)

    return User.objects.filter(wot_id=wot_id).order_by('id').first()


def create_or_update_user(wot_id, username):
    u"""
    Создает или возвращает п
    """
    assert isinstance(wot_id, int) and wot_id > 0
    assert isinstance(username, basestring) and username

    user = get_user_by_wot_id(wot_id)
    if not user:
        user = User(wot_id=wot_id, username=username)
    else:
        user.username = username

    user.save()

    return user


def update_access_token(user, access_token, expires_at):
    u""" """
    assert isinstance(user, User)
    assert isinstance(access_token, basestring) and access_token
    assert isinstance(expires_at, datetime.datetime)

    user.access_token = access_token
    user.access_token_expire = expires_at

    user.save()

    return user


def clear_access_token(user):
    u"""
    Очищает ключ доступ
    """
    assert isinstance(user, User)
    user.access_token = None
    user.access_token_expire = None
    user.save()


def access_token_is_active(user):

    now = datetime.datetime.utcnow()
    now = now.replace(tzinfo=pytz.utc)

    return user.access_token and user.access_token_expire and user.access_token_expire > now
