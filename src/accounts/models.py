# coding:utf-8

from __future__ import unicode_literals
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models

__author__ = 'akvarats'


class UserManager(BaseUserManager):

    def create_user(self, email):
        raise NotImplementedError()

    def create_superuser(self, email):
        raise NotImplementedError()


class User(AbstractBaseUser):
    u"""
    Модель пользователя
    """
    username = models.CharField(max_length=100, unique=True)

    is_superuser = models.BooleanField(default=False)

    # временное смещение текущего пользователя (ну, почти GMT) в минутах
    gmt = models.SmallIntegerField(default=0)

    wot_id = models.PositiveIntegerField(default=0)
    access_token = models.CharField(max_length=100, null=True)
    access_token_expire = models.DateTimeField(null=True)

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        db_table = 'users'
