# coding:utf-8

from __future__ import unicode_literals
from accounts.users import clear_access_token
from core.response import json_response
from core.wot_api import wot_auth_logout
from django.contrib.auth import logout
from django.views.generic import View

__author__ = 'akvarats'


class UserSessionView(View):
    u"""
    Работа с сессией текущего пользователя
    """

    def get(self, request):

        user = None
        if request.user and request.user.is_authenticated():
            user = dict(
                id=request.user.id,
                name=request.user.username
            )

        return json_response(dict(user=user))

    def delete(self, request):

        if request.user and request.user.is_authenticated():
            if request.user.access_token:
                wot_auth_logout(request.user.access_token)
                clear_access_token(request.user)
            logout(request)

        return json_response(dict(status='ok'))
