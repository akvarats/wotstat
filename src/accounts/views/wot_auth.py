# coding:utf-8

from __future__ import unicode_literals
from accounts.users import get_user_by_wot_id, create_or_update_user, update_access_token
from django.contrib.auth import authenticate, login
from django.db import transaction

import requests
import datetime

from core.wot_api import wot_auth_login, WotApiException

from core.response import json_response, bad_request_response, redirect_response
from django.views.generic import View

__author__ = 'akvarats'


class WotAuthLoginView(View):
    u"""

    """

    def get(self, request):

        # создаем токен для входа внутрь нашего приложения
        try:
            data = wot_auth_login()
        except WotApiException:
            return bad_request_response(u'Проблемы с доступом к API WoT')

        if data.get('status') != 'ok':
            return bad_request_response('Не удалось подключиться к серверу WoT')

        url = data.get('data').get('location')

        return json_response(dict(url=url))


class WotAuthLogonView(View):
    u""" """

    def get(self, request):

        status = request.GET.get('status')

        if status == 'ok':
            access_token = request.GET.get('access_token')
            expires_at = datetime.datetime.utcfromtimestamp(int(request.GET.get('expires_at')))
            wot_id = int(request.GET.get('account_id'))
            nickname = request.GET.get('nickname')

            with transaction.atomic():
                user = create_or_update_user(wot_id=wot_id, username=nickname)
                update_access_token(user, access_token, expires_at)

                user = authenticate(wot_id=wot_id)
                if user:
                    login(request, user)

        return redirect_response('/')


