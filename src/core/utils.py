# coding: utf-8

from __future__ import unicode_literals
import datetime
import pytz

__author__ = 'akvarats'


def from_unix_utc_datetime(utc_timestamp):
    return datetime.datetime.fromtimestamp(utc_timestamp, tz=pytz.utc)


def utc_now():
    u"""
    Возвращает текущее время по UTC
    """
    return datetime.datetime.utcnow()


def is_real_date(dt, min_year=1901):
    return isinstance(dt, (datetime.datetime, datetime.date)) and dt.year >= min_year