# coding:utf-8
from __future__ import unicode_literals
import urlparse

import requests
from django.conf import settings

__author__ = 'akvarats'

WARGAMING_API_AUTH = 'https://api.worldoftanks.ru/wot/'


class WotApiException(Exception):
    pass


def wot_auth_login():
    u"""
    Отправляет запрос на получение ссылки для авторизации в варгейминге
    """

    url = urlparse.urljoin(WARGAMING_API_AUTH, 'auth/login/')

    response = requests.get(url, params=dict(
        application_id=settings.WOT_APPLICATION_ID,
        nofollow='1',
        redirect_uri=urlparse.urljoin(settings.DOMAIN_URL, 'api/wot-auth/logon')
    ))

    if response.status_code != 200:
        raise WotApiException('auth/login responsed with status_code %i' % response.status_code)

    return response.json()


def wot_auth_logout(access_token):
    assert isinstance(access_token, basestring)

    url = urlparse.urljoin(WARGAMING_API_AUTH, 'auth/logout/')

    response = requests.get(url, params=dict(
        application_id=settings.WOT_APPLICATION_ID,
        access_token=access_token
    ))

    if response.status_code != 200:
        raise WotApiException('auth/logout/ responsed with status_code %i' % response.status_code)

    return response.json()


def wot_auth_prolongate(access_token):
    u"""
    Продлевает аксесс токен пользователя
    """

    url = urlparse.urljoin(WARGAMING_API_AUTH, 'auth/prolongate/')

    response = requests.post(url, data=dict(
        application_id=settings.WOT_APPLICATION_ID,
        access_token=access_token
    ))

    if response.status_code != 200:
        raise WotApiException('auth/prolongate/ responsed with status_code %i' % response.status_code)

    return response.json()


def wot_tanks():
    u"""
    Отправляет запрос на получение списка танков
    """

    url = urlparse.urljoin(WARGAMING_API_AUTH, 'encyclopedia/vehicles/')

    response = requests.get(url, params=dict(
        application_id=settings.WOT_APPLICATION_ID,
        fields='tank_id, is_premium, images, type, short_name, name, nation, tier'
    ))

    if response.status_code != 200:
        raise WotApiException('auth/login responsed with status_code %i' % response.status_code)

    return response.json()


def user_stat(wot_user_id):
    u"""
    Возвращает статистику по игроку
    """
    assert isinstance(wot_user_id, int)

    url = urlparse.urljoin(WARGAMING_API_AUTH, 'account/info/')
    response = requests.get(url, params=dict(
        application_id=settings.WOT_APPLICATION_ID,
        account_id=wot_user_id,
        fields='statistics.all, global_rating, logout_at, last_battle_time',
    ))

    if response.status_code != 200:
        raise WotApiException('account/info/ responsed with status_code %i' % response.status_code)

    return response.json()


def user_tanks_stat(wot_user_id):
    u""" """
    assert isinstance(wot_user_id, int)

    url = urlparse.urljoin(WARGAMING_API_AUTH, 'tanks/stats/')

    response = requests.get(url, params=dict(
        application_id=settings.WOT_APPLICATION_ID,
        account_id=wot_user_id,
        fields='all, tank_id, max_xp, max_frags, mark_of_mastery'
    ))

    if response.status_code != 200:
        raise WotApiException('tanks/stats/ responsed with status_code %i' % response.status_code)

    return response.json()
