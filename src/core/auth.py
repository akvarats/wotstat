# coding:utf-8

from __future__ import unicode_literals
from core.response import forbidden_response

__author__ = 'akvarats'


def check_api_auth(view_fn):

    def wrapper(self, request):
        if not request.user.is_authenticated():
            return forbidden_response()
        return view_fn(self, request)
    return wrapper