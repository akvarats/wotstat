#coding:utf-8
import json
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound, HttpResponseBadRequest, \
    HttpResponseRedirect


__author__ = 'akvarats'


def json_response(data):
    u""" Хелпер, который превращает json в http response с mimetype application/json """
    assert isinstance(data, (dict, list, tuple))

    return HttpResponse(content=json.dumps(data), content_type='application/json')


def html_response(html):
    u""" Хелпер, который возвращает указанный HTML на клиент"""
    assert isinstance(html, basestring)
    return HttpResponse(html, content_type='text/html')


def forbidden_response(msg=None):
    return HttpResponseForbidden(
        content=json.dumps(dict(success=False, msg=msg or u'Access denied')),
        content_type='application/json'
    )


def notfound_response(msg=None):
    return HttpResponseNotFound(
        content=json.dumps(dict(success=False, msg=msg or 'Not found')),
        content_type='application/json'
    )


def not_found_response(msg=None):
    return notfound_response(msg)


def success_false_response(msg=None):
    if msg is not None:
        result = dict(success=False, msg=msg)
    else:
        result = dict(success=False)
    return json_response(result)


def unknown_method_response():
    return HttpResponseBadRequest(
        content=json.dumps(dict(success=False, msg='Unknown server method')),
        content_type='application/json'
    )


def bad_request_response(msg=None):
    return HttpResponseBadRequest(
        content=json.dumps(dict(success=False, msg=msg or 'Bad request')),
        content_type='application/json'
    )


def redirect_response(redirect_url):
    return HttpResponseRedirect(redirect_url)