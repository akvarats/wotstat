# coding:utf-8

from __future__ import unicode_literals

import json
import datetime

from core.casts import to_integer, to_date, to_time, to_datetime, to_bool, to_dict
from django.http.request import HttpRequest


def request_body(request):
    """
    """
    assert isinstance(request, HttpRequest)

    result = None
    if 'application/json' in request.META.get('CONTENT_TYPE', ''):
        result = json.loads(request.body) if request.body else None
    else:
        result = request.GET if request.method == 'GET' else request.POST

    return result or dict()


def request_has_param(request, name):
    """
    """
    assert isinstance(request, (HttpRequest, dict))
    assert isinstance(name, basestring)

    body = request_body(request) if isinstance(request, HttpRequest) else request

    return name in body


def request_param(request, name, type, default=None):
    """
    """
    assert isinstance(request, (HttpRequest, dict))
    assert isinstance(name, basestring)
    assert type

    body = request_body(request) if isinstance(request, HttpRequest) else request

    if name not in body:
        return default

    raw_value = body[name]

    if type in (int, 'int'):
        return to_integer(raw_value)

    elif type in (unicode, str, basestring, 'unicode', str):
        return unicode(raw_value) if raw_value else raw_value

    elif type in (datetime.date, 'date'):
        return to_date(raw_value)

    elif type in (datetime.datetime, 'datetime'):
        return to_datetime(raw_value)

    elif type in (datetime.time, 'time'):
        return to_time(raw_value)

    elif type in (bool, 'bool', 'boolean'):
        return to_bool(raw_value)

    elif type in (object, 'obj', 'object'):
        to_dict(raw_value)

    return default