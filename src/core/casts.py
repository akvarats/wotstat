# coding:utf-8

from __future__ import unicode_literals

import datetime
import json
from decimal import Decimal, ROUND_HALF_UP

from core.utils import is_real_date


def to_integer(value):
    """
    """
    return int(value or None)


def to_decimal(value, digits=None):
    """
    """
    if not value:
        result = Decimal('0')
    elif isinstance(value, float):
        result = Decimal('%s' % value)
    else:
        result = Decimal(value)

    if digits is not None:
        result = result.quantize(Decimal('1.' + '0' * digits), rounding=ROUND_HALF_UP)


def to_date(value, format=None):
    """
    """
    result = None
    if value:
        if isinstance(value, basestring):
            try:
                result = datetime.datetime.strptime(value, format or '%d.%m.%Y').date()
            except (ValueError, TypeError):
                pass
        elif isinstance(value, datetime.date):
            result = value
        elif isinstance(value, datetime.datetime):
            result = value.date()

    return result


def to_datetime(value, format=None):
    """
    """
    result = None
    if value:
        if isinstance(value, basestring):
            if format is None:
                format = '%d.%m.%Y %H:%M:%S'
            try:
                result = datetime.datetime.strptime(value, format)
            except (ValueError, TypeError):
                pass
        elif isinstance(value, datetime.date):
            result = datetime.datetime.combine(value, datetime.time.min)
        elif isinstance(value, datetime.datetime):
            result = value

    return result


def to_time(value, format=None):
    """
    Преобразует значение value к типу datetime.time

    :param value: значение, которое нужно привести к временнОму типу
    :param format: формат значения для случая, когда value указывается в виде строки
    :rtype: datetime.time
    """

    result = None

    if value:
        if isinstance(value, basestring):
            if format is None:
                # конкретный формат не указан, пробуем подобрать формат сами
                format = '%H:%M' if value.count(':') == 1 else '%H:%M:%S'
            try:
                d = datetime.datetime.strptime(value, format)
                result = datetime.time(d.hour, d.minute, d.second)
            except (TypeError, ValueError):
                pass
        elif isinstance(value, datetime.datetime):
            result = datetime.time(value.hour, value.minute, value.second)
        elif isinstance(value, datetime.date):
            result = datetime.time(0, 0, 0)

    return result


def to_bool(value):
    """
    Приведение к типу boolean
    """
    return value in ('true', 'True', '1', 'on') if isinstance(value, basestring) else bool(value)


def to_dict(value):
    """
    Приведение к JSON объекту
    """
    result = None
    if isinstance(value, dict):
        result = value
    elif isinstance(value, basestring):
        try:
            result = json.loads(value)
        except (TypeError, ValueError):
            pass

    return result


def datetime_to_string(dt, format=None):
    """
    """
    assert isinstance(dt, (datetime.datetime, datetime.date)) or dt is None

    if format is None:
        format = '%d.%m.%Y' if isinstance(dt, datetime.date) else '%d.%m.%Y %H:%M:%S'

    return dt.strftime(format) if is_real_date(dt) else None
