/**
 * Настройка сборки corebone.min.js файла
 */
({
    baseUrl: '.',
    optimize: 'none',
    name: 'main',
    out: 'wotstat.js',
    mainConfigFile: 'main.js'
})