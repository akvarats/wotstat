/**
 * Created by akvarats on 16.02.16.
 */
define(['corebone', 'models'], function(corebone, models) {

    var MainMenu = corebone.Widget.extend({

        className: 'menu__main',

        user: undefined,

        events: function() {
            return _.extend({}, corebone.Widget.prototype.events, {
                'click [data-action="login"]': 'onLoginClick',
                'click [data-action="logout"]': 'onLogoutClick'
            });
        },

        initialize: function(options) {
            corebone.Widget.prototype.initialize.call(this, options);

            _.extend(this, _.pick(options, 'user'));
        },

        render: function() {

            this.$el.html('\
                <div class="ui inverted container">\
                    <div class="ui inverted secondary pointing menu">\
                        <a  data-item="stat-account" class="item">Статистика по аккаунту</a>\
                        <a data-item="stat-tank"class="item">Статистика по танкам</a>\
                        <a data-item="hd" class="item">Поддержка</a>\
                        <div class="right menu">\
                            <div class="ui dropdown item">\
                                ' + this.user.name + '\
                                <i class="dropdown icon"></i>\
                                <div class="menu">\
                                    <a class="item">Выйти</a>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            ');

            return this;
        },

        onLoginClick: function() {
            this.parent.showMask();
            var wotAuth = new models.WotAuth();
            wotAuth.fetch()
                .always(_.bind(function() { this.parent.hideMask(); }, this))
                .done(_.bind(function() {
                    window.location.href = wotAuth.get('url');
                }, this))
        },

        onLogoutClick: function() {
            this.parent.showMask();

            if (window.ServerData.user) {
                var userSession = new models.UserSession({id: 0});
                userSession.destroy()
                    .done(_.bind(function() {
                        window.location.href = '/';
                    }, this))
                    .fail(_.bind(function() {
                        this.parent.hideMask();
                        noty({
                            text: '<h4>Ошибка выхода</h4><div>Выход из системы временно невозможен. Попробуйте позднее.</div>',
                            layout: 'topRight',
                            type: 'warning'
                        });
                    }, this))
            }
        },

        setActiveItem: function(item) {
            this.$('.menu a').removeClass('active');

            this.$('.menu a[data-item="' + item + '"]').addClass('active');
        }

    });

    return MainMenu;
});
