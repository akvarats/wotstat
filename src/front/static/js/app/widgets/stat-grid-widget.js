/**
 * Created by akvarats on 11.03.16.
 */
define(['corebone'], function(Corebone) {

    var StatGridWidget = Corebone.Widget.extend({

        initialize: function(options) {
            Corebone.Widget.prototype.initialize.call(this, options);

            _.extend(this, _.pick(options, 'collection'));
        },

        render: function() {
            return this;
        }

    });

    return StatGridWidget;
});
