/**
 * Created by akvarats on 08.03.16.
 */
define(['corebone', 'utils'], function(corebone, utils) {

    var StatisticsWidget = corebone.Widget.extend({

        className: 'stat-widget',

        initialize: function(options) {
            corebone.Widget.prototype.initialize.call(this, options);
            _.extend(this, _.pick(options, 'winrate', 'battles', 'wg', 'wn8', 'eff'))
        },

        render: function() {
            this.refresh();

            return this;
        },

        $statItem: function(value, label, colorFn, valueFn) {

            var $el = $('\
                <div class="ui inverted statistic">\
                    <div class="value">' + (_.isFunction(valueFn) ? valueFn(value) : value || '-' ) + '</div>\
                    <div class="label">' + label + '</div>\
                </div>\
            ');
            if (_.isFunction(colorFn)) {
                $el.addClass(colorFn(value));
            }
            return $el;
        },

        refresh: function(options) {
            if (options) {
                _.extend(this, _.pick(options, 'winrate', 'battles', 'wg', 'wn8', 'eff'));
            }

            this.$el.empty();

            if (this.battles) this.$el.append(this.$statItem(this.battles, 'боев', utils.Stat.battlesColor, utils.Numbers.formatNumber));
            if (this.winrate) this.$el.append(this.$statItem(this.winrate, '% побед', utils.Stat.winrateColor));
            if (this.wg) this.$el.append(this.$statItem(this.wg, 'рейтинг wg', utils.Stat.wgColor, utils.Numbers.formatNumber));
            if (this.wn8) this.$el.append(this.$statItem(this.wn8, 'wn8', utils.Stat.wn8Color, utils.Numbers.formatNumber));
            if (this.eff) this.$el.append(this.$statItem(this.eff, 'эфф', utils.Stat.effColor, utils.Numbers.formatNumber));
        }

    });

    return StatisticsWidget;
});
