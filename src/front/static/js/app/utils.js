/**
 * Created by akvarats on 08.03.16.
 */
define([], function() {

    var Stat = {
        /**
         * Возвращает цвет, соответствующий показателю количества боев
         * @param value
         */
        winrateColor: function (value) {
            if (value < 46.5) return 'red';
            if (value < 48.5) return 'orange';
            if (value < 52.5) return 'olive';
            if (value < 57.5) return 'green';
            if (value < 64.5) return 'blue';
            return 'purple';
        },

        battlesColor: function(value) {
            if (value < 2000) return 'red';
            if (value < 5000) return 'orange';
            if (value < 9000) return 'olive';
            if (value < 14000) return 'green';
            if (value < 50000) return 'blue';
            return 'purple';
        },

        wgColor: function(value) {
            if (value < 2495) return 'red';
            if (value < 4345) return 'orange';
            if (value < 6425) return 'olive';
            if (value < 8625) return 'green';
            if (value < 10040) return 'blue';
            return 'purple';
        },

        wn8Color: function(value) {
            if (value < 380) return 'red';
            if (value < 860) return 'orange';
            if (value < 1420) return 'olive';
            if (value < 2105) return 'green';
            if (value < 2770) return 'blue';
            return 'purple';
        },

        effColor: function(value) {
            if (value < 615) return 'red';
            if (value < 870) return 'orange';
            if (value < 1175) return 'olive';
            if (value < 1525) return 'green';
            if (value < 1850) return 'blue';
            return 'purple';
        }
    };

    var Numbers = {
        formatNumber: function(value) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        }
    };

    return {
        Stat: Stat,
        Numbers: Numbers
    }
});
