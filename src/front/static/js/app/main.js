/**
 * Created by akvarats on 06.01.16.
 */
require.config({
    waitSeconds: 30,
    paths: {
        // ---------- внешние библиотеки -----------------
        'corebone': '../libs/corebone.min',
        'semantic': '../libs/semantic',
        'noty': '../libs/noty',

        // ----------- приложение ---------------
        'router': 'router',
        'application': 'application',
        'viewport': 'viewport',
        'models': 'models',
        'utils': 'utils',

        // --------- страницы ------------------
        'base-page': 'pages/base-page',
        'index-page': 'pages/index-page',
        'layout-page': 'pages/layout-page',
        'grid-page': 'pages/grid-page',

        'stat-base-page': 'pages/stat-base-page',
        'account-stat-summary-page': 'pages/account-stat-summary-page',

        // ------------------ виджеты приложения --------------
        'main-menu': 'widgets/main-menu',
        'stat-widget': 'widgets/stat-widget'
    }
});

require(['application', 'semantic', 'models', 'noty', 'base-page'], function(application) {

    application.initialize();

});
