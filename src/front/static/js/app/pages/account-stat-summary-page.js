/**
 * Created by akvarats on 07.03.16.
 */
define(['corebone', 'stat-base-page', 'stat-widget', 'models'], function(corebone, StatBasePage, StatWidget, models) {

    var AccountStatSummaryPage = StatBasePage.extend({

        menuItem: 'stat-account',

        statWidget: undefined,

        render: function() {

            var statData = {
                battles: 23749,
                winrate: 46.73,
                wg: 3048,
                wn8: 4554,
                eff: 4701
            };

            this.statWidget = this.child(StatWidget, statData);

            this.$statWidgetWrapper = $('<div class="bMrgn15" />');
            this.$statControlWrapper = $('<div class="bMrgn15" />');
            this.$gridWrapper = $('<div />');

            this.$el.append(this.$statWidgetWrapper);
            this.$el.append(this.$statControlWrapper);
            this.$el.append(this.$gridWrapper);

            //this.$el.append(.render().$el);

            //this.$el.append('<div class="bMrgn15 tMrgn15">Последнее обновление рейтингов: 08.03.2016 15:56 (<a data-action="refresh-stats" href="#">обновить</a>)</div>');

            /*this.$el.append('\
                <table class="ui inverted selectable table wotstat__grid">\
                    <thead>\
                        <th>Дата</th>\
                        <th>Бои</th>\
                        <th>%</th>\
                        <th>WG</th>\
                        <th>WN8</th>\
                        <th>Эфф.</th>\
                        <th>Урон</th>\
                        <th>Опыт</th>\
                        <th>Фраги</th>\
                        <th>База</th>\
                    </thead>\
                    <tbody>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                        <tr>\
                            <td>08.03.2016</td>\
                            <td>10 456 </td>\
                            <td>56.76% <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.21</span></td>\
                            <td><span style="color: #DC73FF">3 048</span> <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>4 554 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>4 701 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.01</span></td>\
                            <td>757 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>15.03</span></td>\
                            <td>343 <span style="color: #ff695e; font-size: 12px;"><i class="arrow down icon"></i>15.03</span></td>\
                            <td>1.11 <span style="color: #21ba45; font-size: 12px;"><i class="arrow up icon"></i>0.01</span></td>\
                            <td>0.75 / 0.58</td>\
                        </tr>\
                    </tbody>\
                </table>\
            ');*/
            return this;
        },

        activate: function() {
            // страница становится активной на экране пользователя

            this.statWidget = this.child(StatWidget);

            this.$statWidgetWrapper.html(this.statWidget.render().$el);

            this.showMask();
            var latestStats = new models.StatsAccountLatest();

            latestStats.fetch()
                .always(_.bind(function() { this.hideMask(); }, this))
                .done(_.bind(function() {
                    this.statWidget.refresh(latestStats.get('stats'));
                    this.$statControlWrapper.html('Последнее обновление: ' + latestStats.get('updated_at') + ' <a href="#">Обновить сейчас</a>');
                }, this))
                .fail(_.bind(function() {
                    this.showError('Не удалось получить статистику по аккаунту. Попробуйте позднее');
                }, this));

            //this.statWidget = this.child(StatWidget);
        }

    });

    return AccountStatSummaryPage;
});
