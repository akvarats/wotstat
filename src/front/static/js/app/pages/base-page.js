/**
 * Created by akvarats on 13.03.16.
 */
define(['corebone'], function(Corebone) {

    var WotstatPage = Corebone.Page.extend({

        activate: function() {
            _.each(this)
        },

        showMask: function() {
            this.$el.append('<div class="ui active dimmer" data-el="page-mask"><div class="ui loader"></div></div>');
        },

        hideMask: function() {
            this.$('[data-el="page-mask"]').remove();
        },

        showError: function(text) {
            noty({
                type: 'error',
                layout: 'topRight',
                text: text,
                timeout: 3000
            })
        },

        showWarning: function(text) {
            noty({
                type: 'warning',
                layout: 'topRight',
                text: text,
                timeout: 3000
            })
        },

        showMessage: function(text) {
            noty({
                type: 'info',
                layout: 'topRight',
                text: text,
                timeout: 3000
            })
        }

    });

    return WotstatPage;
});
