/**
 * Created by akvarats on 02.03.16.
 */
define(['corebone', 'models'], function(corebone, models) {

    var IndexPageNotAuth = corebone.Page.extend({

        className: 'page__index',

        events: function() {
            return _.extend({}, corebone.Widget.prototype.events, {
                'click [data-action="login"]': 'onLoginClick',
                'click [data-action="login-confirmed"]': 'onLoginConfirmed',
                'click [data-action="login-cancelled"]': 'onLoginCancelled'
            });
        },

        render: function() {

            this.$el.html('\
                <h1 class="ui header">Сервис сбора личной статистики</h1>\
                <div><button data-action="login" class="ui primary button">Начать</button></div>\
            ');

            return this;
        },

        renderLoginConfirmDialog: function() {

            var modalEl = $('\
                <div data-el="login-confim-modal" class="ui inverted modal">\
                     <div class="header">\
                        Вход в сервис\
                     </div>\
                     <div class="content">\
                        Для входа в систему мы используем учетные записи Wargaming.\
                     </div>\
                     <div class="actions">\
                        <button data-action="login-confirmed" class="ui positive icon button">\
                            <i class="checkmark icon"></i> Продолжить\
                        </button>\
                        <button data-action="login-cancelled" class="ui deny black button">Отмена</button>\
                     </div>\
                </div>\
            ');

            this.$el.append(modalEl);

            return modalEl;
        },

        onLoginClick: function() {
            this.renderLoginConfirmDialog().modal({
                onApprove: _.bind(this.onLoginConfirmed, this)
            }).modal('show');
        },

        onLoginConfirmed: function() {
            this.parent.showMask();
            var wotAuth = new models.WotAuth();
            wotAuth.fetch()
                .done(_.bind(function() {
                    window.location.href = wotAuth.get('url');
                }, this))
                .fail(_.bind(function() {
                    this.parent.hideMask();
                    noty({
                        text: '<h4>Ошибка входа</h4><div>Вход в систему временно невозможен. Попробуйте позднее.</div>',
                        layout: 'topRight',
                        type: 'warning'
                    });
                }, this))
        },

        onLoginCancelled: function() {
            this.$('.modal').modal('hide');
        }

    });

    return IndexPageNotAuth;
});