/**
 * Created by akvarats on 08.01.16.
 */
define(['corebone'], function(corebone) {

    var LayoutPage = corebone.Page.extend({

        render: function() {
            this.$el.html('Страница с лейаутами');
            return this;
        }

    });

    return LayoutPage;
});
