/**
 * Created by akvarats on 07.01.16.
 */
define(['corebone'], function(corebone) {

    var GridPage = corebone.Page.extend({

        render: function() {
            this.$el.html('Тут будет страница с гридом');
        }

    });

    return GridPage;
});