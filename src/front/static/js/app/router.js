/**
 * Created by akvarats on 06.01.16.
 */
define(['corebone'], function(corebone) {

    var Router = corebone.Router.extend({

        routes: {
            'account-stat-summary-page': 'accountStatSummaryPage',
            'grid': 'grid',
            'layout': 'layout',
            '*options': 'index'
        },

        index: function(options) {

            if (!this._isAuthenticated()) {
                this.viewport.renderPage('pages/index-page-not-auth');
                return;
            }

            this.viewport.renderPage('pages/account-stat-summary-page');
        },

        accountStatSummaryPage: function() {
            this.viewport.renderPage('pages/account-stat-summary-page');
        },

        layout: function() {
            this.viewport.renderPage('layout-page');
        },

        grid: function() {
            this.viewport.renderPage('grid-page');
        },



        /**
         * Возвращает true в случае, если текущий пользователь авторизован
         * @returns {*}
         * @private
         */
        _isAuthenticated: function() {
            return window.ServerData && window.ServerData.user;
        }

    });

    return Router;
});
