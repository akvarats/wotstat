/**
 * Created by akvarats on 06.01.16.
 */
define(['corebone'], function(corebone) {

    var WotstatModel = corebone.Backbone.Model.extend({});
    var WotstatCollection = corebone.Backbone.Collection.extend({});

    var WotAuth = WotstatModel.extend({
        url: '/api/wot-auth/login'
    });

    var UserSession = WotstatModel.extend({
        url: '/api/user-session'
    });

    var StatsAccountLatest = WotstatModel.extend({
        url: '/api/stats/account-latest'
    });

    var StatsAccountHistoryDetail = WotstatModel.extend({

    });

    var StatsAccountHistoryCollection = WotstatCollection.extend({
        model: StatsAccountHistoryDetail,
        url: '/api/stats/account-history'
    });

    return {
        WotAuth: WotAuth,
        UserSession: UserSession,

        // модели для получения статистики
        StatsAccountLatest: StatsAccountLatest,
        StatsAccountHistoryDetail: StatsAccountHistoryDetail,
        StatsAccountHistoryCollection: StatsAccountHistoryCollection
    }

});
