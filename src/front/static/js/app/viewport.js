/**
 * Created by akvarats on 06.01.16.
 */
define(['corebone', 'main-menu'], function(corebone, MainMenu) {

    var Viewport = corebone.Viewport.extend({

        _pageLoaderHash: undefined,

        render: function() {

            var currentUser = window.ServerData.user;

            // определяем контейнер для вьюх
            this.$container = $('<div class="ui inverted container" data-placeholder="page-container"/>');

            if (currentUser) {
                this.mainMenu = this.child(MainMenu, {user: window.ServerData.user});
                this.$el.append(this.mainMenu.render().$el);
                this.$el.append(this.$container);
            }
            else {
                this.$el.append(this.$container);
            }

            return this;
        },

        getPageContainer: function() {
            return this.$container;
        },

        onUndefinedView: function() {

            // Очищаем контейнер
            this.getPageContainer().empty();

            _.delay(_.bind(function() {
                if (this.getPageContainer().is(':empty')) {
                    // Прошло 200 мс, а вьюха до сих пор не загрузилась. Рисуем прелоадер
                    this.getPageContainer().html('\
                    <div class="ui inverted segment">\
                        <div class="ui active indeterminate inverted loader"></div>\
                    </div>\
                ');
                }
            }, this), 200);
        },

        onPageRendered: function(pageName, page, pageModule) {
            this.mainMenu.setActiveItem(page.menuItem);
        },

        showMask: function() {
            this.$el.append(
                '<div class="ui active dimmer" data-el="viewport-mask"><div class="ui loader"></div></div>'
            )
        },

        hideMask: function() {
            this.$('[data-el="viewport-mask"]').remove();
        }

    });

    return Viewport;
});
