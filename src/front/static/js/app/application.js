/**
 * Created by akvarats on 06.01.16.
 */
define(['corebone', 'router', 'viewport'], function(corebone, Router, Viewport) {

    var Application = corebone.Application.extend({

        timezoneOffset: 0,
        clockCorrection: 0,

        initialize: function(options) {
            corebone.Application.prototype.initialize.call(this, options);

            // корректируем часы на клиенте
            this.timezoneOffset = -1 * new Date().getTimezoneOffset();

            return this;
        }

    });

    var viewport = new Viewport({el: $('body')});
    var router = new Router({viewport: viewport});

    return new Application({router: router, viewport: viewport});
});
