# coding:utf-8

from django.conf.urls import url
from front.views.home import HomeView

__author__ = 'akvarats'

urlpatterns = [
    url(r'^$', HomeView.as_view())
]