# coding:utf-8

from __future__ import unicode_literals
from accounts.users import access_token_is_active
from django.contrib.auth import logout
from django.shortcuts import render_to_response

from django.views.generic import View

__author__ = 'akvarats'


class HomeView(View):
    u"""
    Вьюха для фрейма приложения
    """
    def get(self, request):

        user = None
        if request.user and request.user.is_authenticated():
            if access_token_is_active(request.user):
                user = request.user
            else:
                logout(request)

        return render_to_response('master.html', dict(
            user=user
        ))